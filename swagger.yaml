---
swagger: "2.0"
info:
  title: Koha REST API
  version: "1"
  license:
    name: GPL v3,
    url: http://www.gnu.org/licenses/gpl.txt
  contact:
    name: Koha Development Team
    url: https://koha-community.org/
  description: |
    ## Introduction

    This API is documented in **OpenAPI format**.

    ## Authentication

    The API supports the following authentication mechanisms

    * HTTP Basic authentication
    * OAuth2 (client credentials grant)
    * Cookie-based

    Both _Basic authentication_ and the _OAuth2_ flow, need to be enabled
    by system preferences.

    ## Errors

    The API uses standard HTTP status codes to indicate the success or failure
    of the API call. The body of the response will be JSON in the following format:

    ```
    {
      "error": "Current settings prevent the passed due date to be applied",
      "error_code": "invalid_due_date"
    }
    ```

    Note: Some routes might offer additional attributes in their error responses but that's
    subject to change and thus not documented.

    ## Filtering responses

    The API allows for some advanced response filtering using a JSON based query syntax. The
    query can be added to the requests:

    * as a query parameter `q=`
    * in the request body
    * in a special header `x-koha-query`

    For simple field equality matches we can use `{ "fieldname": "value" }` where the fieldname
    matches one of the fields as described in the particular endpoints response object.

    We can refine that with more complex matching clauses by nesting a the clause into the
    object; `{ "fieldname": { "clause": "value" } }`.

    Available matching clauses include ">", "<", ">=", "<=", "-like", and "-not_like".

    We can filter on multiple fields by adding them to the JSON respresentation. Adding at `HASH`
    level will result in an 'AND' query, whilst combinding them in an `ARRAY` wilth result in an
    'OR' query: `{ "field1": 'value2', "field2": "value2" }` will filter the response to only those
    results with both field1 containing value2 AND field2 containing value2 for example.

    ### Examples

    The following request would return any patron with firstname "Henry" and lastname "Acevedo";

    `curl -u koha:koha --request GET 'http://127.0.0.1:8081/api/v1/patrons/' --data-raw '{ "surname": "Acevedo", "firstname": "Henry" }'`

    The following request would return any patron whose lastname begins with "Ace";

    `curl -u koha:koha --request GET 'http://127.0.0.1:8081/api/v1/patrons/' --data-raw '{ "surname": { "-like": "Ace%" }'`

    The following request would return any patron whilse lastname is 'Acevedo' OR 'Bernardo'

    `curl -u koha:koha --request GET 'http://127.0.0.1:8081/api/v1/patrons/' --data-raw '{ "surname": [ "Acevedo", "Bernardo" ] }'`

    ## Special headers

    ### x-koha-library

    This optional header should be passed to give your api request a library
    context; If it is not included in the request, then the request context
    will default to using your api comsumer's assigned home library.
basePath: /api/v1
paths:
  /oauth/token:
    $ref: "./paths/oauth.yaml#/~1oauth~1token"
  /acquisitions/orders:
    $ref: "./paths/acquisitions_orders.yaml#/~1acquisitions~1orders"
  "/acquisitions/orders/{order_id}":
    $ref: "./paths/acquisitions_orders.yaml#/~1acquisitions~1orders~1{order_id}"
  /acquisitions/vendors:
    $ref: "./paths/acquisitions_vendors.yaml#/~1acquisitions~1vendors"
  "/acquisitions/vendors/{vendor_id}":
    $ref: "./paths/acquisitions_vendors.yaml#/~1acquisitions~1vendors~1{vendor_id}"
  /acquisitions/funds:
    $ref: "./paths/acquisitions_funds.yaml#/~1acquisitions~1funds"
  "/article_requests/{article_request_id}":
    $ref: "./paths/article_requests.yaml#/~1article_requests~1{article_request_id}"
  "/biblios/{biblio_id}":
    $ref: "./paths/biblios.yaml#/~1biblios~1{biblio_id}"
  "/biblios/{biblio_id}/checkouts":
    $ref: "./paths/biblios.yaml#/~1biblios~1{biblio_id}~1checkouts"
  "/biblios/{biblio_id}/items":
    $ref: "./paths/biblios.yaml#/~1biblios~1{biblio_id}~1items"
  "/biblios/{biblio_id}/pickup_locations":
    $ref: "./paths/biblios.yaml#/~1biblios~1{biblio_id}~1pickup_locations"
  "/cash_registers/{cash_register_id}/cashups":
    $ref: "./paths/cash_registers.yaml#/~1cash_registers~1{cash_register_id}~1cashups"
  "/cashups/{cashup_id}":
    $ref: "./paths/cash_registers.yaml#/~1cashups~1{cashup_id}"
  /checkouts:
    $ref: "./paths/checkouts.yaml#/~1checkouts"
  "/checkouts/{checkout_id}":
    $ref: "./paths/checkouts.yaml#/~1checkouts~1{checkout_id}"
  "/checkouts/{checkout_id}/renewal":
    $ref: "./paths/checkouts.yaml#/~1checkouts~1{checkout_id}~1renewal"
  /circulation-rules/kinds:
    $ref: "./paths/circulation-rules.yaml#/~1circulation-rules~1kinds"
  /cities:
    $ref: "./paths/cities.yaml#/~1cities"
  "/cities/{city_id}":
    $ref: "./paths/cities.yaml#/~1cities~1{city_id}"
  "/clubs/{club_id}/holds":
    $ref: "./paths/clubs.yaml#/~1clubs~1{club_id}~1holds"
  /config/smtp_servers:
    $ref: "./paths/config_smtp_servers.yaml#/~1config~1smtp_servers"
  "/config/smtp_servers/{smtp_server_id}":
    $ref: "./paths/config_smtp_servers.yaml#/~1config~1smtp_servers~1{smtp_server_id}"
  /holds:
    $ref: "./paths/holds.yaml#/~1holds"
  "/holds/{hold_id}":
    $ref: "./paths/holds.yaml#/~1holds~1{hold_id}"
  "/holds/{hold_id}/priority":
    $ref: "./paths/holds.yaml#/~1holds~1{hold_id}~1priority"
  "/holds/{hold_id}/suspension":
    $ref: "./paths/holds.yaml#/~1holds~1{hold_id}~1suspension"
  "/holds/{hold_id}/pickup_locations":
    $ref: "./paths/holds.yaml#/~1holds~1{hold_id}~1pickup_locations"
  "/holds/{hold_id}/pickup_location":
    $ref: "./paths/holds.yaml#/~1holds~1{hold_id}~1pickup_location"
  /items:
    $ref: "./paths/items.yaml#/~1items"
  "/items/{item_id}":
    $ref: "./paths/items.yaml#/~1items~1{item_id}"
  "/items/{item_id}/pickup_locations":
    $ref: "./paths/items.yaml#/~1items~1{item_id}~1pickup_locations"
  /libraries:
    $ref: "./paths/libraries.yaml#/~1libraries"
  "/libraries/{library_id}":
    $ref: "./paths/libraries.yaml#/~1libraries~1{library_id}"
  /transfer_limits:
    $ref: "./paths/transfer_limits.yaml#/~1transfer_limits"
  "/transfer_limits/{limit_id}":
    $ref: "./paths/transfer_limits.yaml#/~1transfer_limits~1{limit_id}"
  /transfer_limits/batch:
    $ref: "./paths/transfer_limits.yaml#/~1transfer_limits~1batch"
  "/checkouts/{checkout_id}/allows_renewal":
    $ref: "./paths/checkouts.yaml#/~1checkouts~1{checkout_id}~1allows_renewal"
  /advanced_editor/macros:
    $ref: "./paths/advancededitormacros.yaml#/~1advanced_editor~1macros"
  "/advanced_editor/macros/{advancededitormacro_id}":
    $ref: "./paths/advancededitormacros.yaml#/~1advanced_editor~1macros~1{advancededitormacro_id}"
  /advanced_editor/macros/shared:
    $ref: "./paths/advancededitormacros.yaml#/~1advanced_editor~1macros~1shared"
  "/advanced_editor/macros/shared/{advancededitormacro_id}":
    $ref: "./paths/advancededitormacros.yaml#/~1advanced_editor~1macros~1shared~1{advancededitormacro_id}"
  /patrons:
    $ref: "./paths/patrons.yaml#/~1patrons"
  "/patrons/{patron_id}":
    $ref: "./paths/patrons.yaml#/~1patrons~1{patron_id}"
  "/patrons/{patron_id}/account":
    $ref: "./paths/patrons_account.yaml#/~1patrons~1{patron_id}~1account"
  "/patrons/{patron_id}/account/credits":
    $ref: "./paths/patrons_account.yaml#/~1patrons~1{patron_id}~1account~1credits"
  "/patrons/{patron_id}/extended_attributes":
    $ref: "./paths/patrons_extended_attributes.yaml#/~1patrons~1{patron_id}~1extended_attributes"
  "/patrons/{patron_id}/extended_attributes/{extended_attribute_id}":
    $ref: "./paths/patrons_extended_attributes.yaml#/~1patrons~1{patron_id}~1extended_attributes~1{extended_attribute_id}"
  "/patrons/{patron_id}/holds":
    $ref: "./paths/patrons_holds.yaml#/~1patrons~1{patron_id}~1holds"
  "/patrons/{patron_id}/password":
    $ref: "./paths/patrons_password.yaml#/~1patrons~1{patron_id}~1password"
  /ill_backends:
    $ref: "./paths/ill_backends.yaml#/~1ill_backends"
  "/ill_backends/{ill_backend_id}":
    $ref: "./paths/ill_backends.yaml#/~1ill_backends~1{ill_backend_id}"
  /illrequests:
    $ref: "./paths/illrequests.yaml#/~1illrequests"
  /import_batch_profiles:
    $ref: "./paths/import_batch_profiles.yaml#/~1import_batch_profiles"
  "/import_batch_profiles/{import_batch_profile_id}":
    $ref: "./paths/import_batch_profiles.yaml#/~1import_batch_profiles~1{import_batch_profile_id}"
  "/rotas/{rota_id}/stages/{stage_id}/position":
    $ref: "./paths/rotas.yaml#/~1rotas~1{rota_id}~1stages~1{stage_id}~1position"
  "/public/biblios/{biblio_id}":
    $ref: "./paths/biblios.yaml#/~1public~1biblios~1{biblio_id}"
  "/public/biblios/{biblio_id}/items":
    $ref: "./paths/biblios.yaml#/~1public~1biblios~1{biblio_id}~1items"
  /public/libraries:
    $ref: "./paths/libraries.yaml#/~1public~1libraries"
  "/public/libraries/{library_id}":
    $ref: "./paths/libraries.yaml#/~1public~1libraries~1{library_id}"
  "/public/patrons/{patron_id}/article_requests/{article_request_id}":
    $ref: "./paths/article_requests.yaml#/~1public~1patrons~1{patron_id}~1article_requests~1{article_request_id}"
  "/public/patrons/{patron_id}/password":
    $ref: "./paths/public_patrons.yaml#/~1public~1patrons~1{patron_id}~1password"
  "/public/patrons/{patron_id}/guarantors/can_see_charges":
    $ref: "./paths/public_patrons.yaml#/~1public~1patrons~1{patron_id}~1guarantors~1can_see_charges"
  "/public/patrons/{patron_id}/guarantors/can_see_checkouts":
    $ref: "./paths/public_patrons.yaml#/~1public~1patrons~1{patron_id}~1guarantors~1can_see_checkouts"
  /quotes:
    $ref: "./paths/quotes.yaml#/~1quotes"
  "/quotes/{quote_id}":
    $ref: "./paths/quotes.yaml#/~1quotes~1{quote_id}"
  /return_claims:
    $ref: "./paths/return_claims.yaml#/~1return_claims"
  "/return_claims/{claim_id}/notes":
    $ref: "./paths/return_claims.yaml#/~1return_claims~1{claim_id}~1notes"
  "/return_claims/{claim_id}/resolve":
    $ref: "./paths/return_claims.yaml#/~1return_claims~1{claim_id}~1resolve"
  "/return_claims/{claim_id}":
    $ref: "./paths/return_claims.yaml#/~1return_claims~1{claim_id}"
  /suggestions:
    $ref: "./paths/suggestions.yaml#/~1suggestions"
  "/suggestions/{suggestion_id}":
    $ref: "./paths/suggestions.yaml#/~1suggestions~1{suggestion_id}"
definitions:
  account_line:
    $ref: "./definitions/account_line.yaml"
  advancededitormacro:
    $ref: "./definitions/advancededitormacro.yaml"
  allows_renewal:
    $ref: "./definitions/allows_renewal.yaml"
  basket:
    $ref: "./definitions/basket.yaml"
  cashup:
    $ref: "./definitions/cashup.yaml"
  checkout:
    $ref: "./definitions/checkout.yaml"
  checkouts:
    $ref: "./definitions/checkouts.yaml"
  circ-rule-kind:
    $ref: "./definitions/circ-rule-kind.yaml"
  city:
    $ref: "./definitions/city.yaml"
  error:
    $ref: "./definitions/error.yaml"
  fund:
    $ref: "./definitions/fund.yaml"
  hold:
    $ref: "./definitions/hold.yaml"
  holds:
    $ref: "./definitions/holds.yaml"
  ill_backend:
    $ref: "./definitions/ill_backend.yaml"
  ill_backends:
    $ref: "./definitions/ill_backends.yaml"
  import_batch_profile:
    $ref: "./definitions/import_batch_profile.yaml"
  import_batch_profiles:
    $ref: "./definitions/import_batch_profiles.yaml"
  invoice:
    $ref: "./definitions/invoice.yaml"
  item:
    $ref: "./definitions/item.yaml"
  library:
    $ref: "./definitions/library.yaml"
  order:
    $ref: "./definitions/order.yaml"
  patron:
    $ref: "./definitions/patron.yaml"
  patron_account_credit:
    $ref: "./definitions/patron_account_credit.yaml"
  patron_balance:
    $ref: "./definitions/patron_balance.yaml"
  patron_extended_attribute:
    $ref: "./definitions/patron_extended_attribute.yaml"
  quote:
    $ref: "./definitions/quote.yaml"
  return_claim:
    $ref: "./definitions/return_claim.yaml"
  smtp_server:
    $ref: "./definitions/smtp_server.yaml"
  suggestion:
    $ref: "./definitions/suggestion.yaml"
  transfer_limit:
    $ref: "./definitions/transfer_limit.yaml"
  vendor:
    $ref: "./definitions/vendor.yaml"
parameters:
  advancededitormacro_id_pp:
    name: advancededitormacro_id
    in: path
    description: Advanced editor macro internal identifier
    required: true
    type: integer
  biblio_id_pp:
    name: biblio_id
    in: path
    description: Record internal identifier
    required: true
    type: integer
  cash_register_id_pp:
    name: cash_register_id
    in: path
    description: Cash register internal identifier
    required: true
    type: integer
  cashup_id_pp:
    name: cashup_id
    in: path
    description: Cashup internal identifier
    required: true
    type: integer
  checkout_id_pp:
    name: checkout_id
    in: path
    description: Internal checkout identifier
    required: true
    type: integer
  city_id_pp:
    name: city_id
    in: path
    description: City internal identifier
    required: true
    type: integer
  club_id_pp:
    name: club_id
    in: path
    description: Internal club identifier
    required: true
    type: integer
  fund_id_pp:
    name: fund_id
    in: path
    description: Fund id
    required: true
    type: integer
  hold_id_pp:
    name: hold_id
    in: path
    description: Internal hold identifier
    required: true
    type: integer
  import_batch_profile_id_pp:
    name: import_batch_profile_id
    in: path
    description: Internal profile identifier
    required: true
    type: integer
  item_id_pp:
    name: item_id
    in: path
    description: Internal item identifier
    required: true
    type: integer
  library_id_pp:
    name: library_id
    in: path
    description: Internal library identifier
    required: true
    type: string
  match:
    name: _match
    in: query
    required: false
    description: Matching criteria
    type: string
    enum:
      - contains
      - exact
      - starts_with
      - ends_with
  order_by:
    name: _order_by
    in: query
    required: false
    description: Sorting criteria
    type: array
    collectionFormat: csv
    items:
      type: string
  order_id_pp:
    name: order_id
    in: path
    description: Internal order identifier
    required: true
    type: integer
  page:
    name: _page
    in: query
    required: false
    description: Page number, for paginated object listing
    type: integer
  patron_id_pp:
    name: patron_id
    in: path
    description: Internal patron identifier
    required: true
    type: integer
  patron_id_qp:
    name: patron_id
    in: query
    description: Internal patron identifier
    type: integer
  per_page:
    name: _per_page
    in: query
    required: false
    description: Page size, for paginated object listing
    type: integer
  q_body:
    name: query
    in: body
    required: false
    description: Query filter sent through request's body
    schema:
      type: object
  q_param:
    name: q
    in: query
    required: false
    description: Query filter sent as a request parameter
    type: string
  q_header:
    name: x-koha-query
    in: header
    required: false
    description: Query filter sent as a request header
    type: string
  quote_id_pp:
    name: quote_id
    in: path
    description: Quote internal identifier
    required: true
    type: integer
  seen_pp:
    name: seen
    in: query
    description: Item was seen flag
    required: false
    type: integer
  smtp_server_id_pp:
    name: smtp_server_id
    in: path
    description: SMTP server internal identifier
    required: true
    type: integer
  suggestion_id_pp:
    name: suggestion_id
    in: path
    description: Internal suggestion identifier
    required: true
    type: integer
  transfer_limit_id_pp:
    name: limit_id
    in: path
    description: Internal transfer limit identifier
    required: true
    type: string
  vendor_id_pp:
    name: vendor_id
    in: path
    description: Vendor id
    required: true
    type: integer
tags:
  - name: "article_requests"
    x-displayName: Article requests
    description: |
      Manage article requests
  - name: "biblios"
    x-displayName: Biblios
    description: |
      Manage bibliographic records
  - name: "cashups"
    x-displayName: Cashups
    description: |
      Manage cash register cashups
  - name: "checkouts"
    x-displayName: Checkouts
    description: |
      Manage checkouts
  - name: "circulation_rules"
    x-displayName: Circulation rules
    description: |
      Manage circulation rules
  - name: "cities"
    x-displayName: Cities
    description: |
      Manage cities
  - name: "clubs"
    x-displayName: Clubs
    description: |
      Manage patron clubs
  - name: "funds"
    x-displayName: Funds
    description: |
      Manage funds for the acquisitions module
  - name: "holds"
    x-displayName: Holds
    description: |
      Manage holds
  - name: "illbackends"
    x-displayName: ILL backends
    description: |
      Manage ILL module backends
  - name: "illrequests"
    x-displayName: ILL requests
    description: |
      Manage ILL requests
  - name: "items"
    x-displayName: Items
    description: |
      Manage items
  - name: "libraries"
    x-displayName: Libraries
    description: |
      Manage libraries
  - name: "macros"
    x-displayName: Macros
    description: |
      Manage macros
  - name: "orders"
    x-displayName: Orders
    description: |
      Manage acquisition orders
  - name: "oauth"
    x-displayName: "OAuth"
    description: |
      Handle OAuth flows
  - name: "patrons"
    x-displayName: Patrons
    description: |
      Manage patrons
  - name: "quotes"
    x-displayName: Quotes
    description: |
      Manage quotes
  - name: "return_claims"
    x-displayName: Return claims
    description: |
      Manage return claims
  - name: "rotas"
    x-displayName: Rotas
    description: |
      Manage rotas
  - name: "smtp_servers"
    x-displayName: SMTP servers
    description: |
      Manage SMTP servers configurations
  - name: "transfer"
    x-displayName: Transfer limits
    description: |
      Manage transfer limits
  - name: "suggestions"
    x-displayName: "Purchase suggestions"
    description: |
      Manage purchase suggestions
  - name: "vendors"
    x-displayName: "Vendors"
    description: |
      Manage vendors for the acquisitions module
  - name: "batch_import_profiles"
    x-displayName: Batch import profiles
    description: |
      Manage batch import profiles
